function GetList() {

    $.ajax({
        url: "/functions/AjaxHandlers.php",
        data: { getAll: 1}
    }).done(function (data) {
            $("#list_content").html(data);
        });
}
function GetListElement(id) {
    $.ajax({
        url: "/functions/AjaxHandlers.php",
        data: { getSingle: id}
    }).done(function (data) {
            $("#list_images").html(data);
        });
}
function Scrape(page_addr) {

    $.ajax({
        url: "/functions/Scrape.php",
        data: { url: page_addr},
        beforeSend: function() {
            $("#list_content").html("Siunčiamas naujas puslapis...Palaukite.");
        }
    }).done(function (data) {
            GetList();
        });
}

$(document).ready(function() {
    console.log("Page loaded");
    GetList();

    $("#scrape_start").click(function() {
       //Start scraping
        page_addr = $("#scrape_page").val();
        console.log(page_addr);
        Scrape(page_addr);
    });
    $("#list").on("click",".show_images",function() {
        id = $(this).attr("id");
        GetListElement(id);
    });






});