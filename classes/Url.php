<?php

class Url {

    var $url;
    var $hash;
    var $content;
    var $databaseID;


    function __construct($url)
    {
        $this->url = $url;
        $this->hash = uniqid();

        $this->createFolder();

        $this->addToDB();
    }

    function isValid() {
        return filter_var($this->url, FILTER_VALIDATE_URL);
    }

    function open() {
        require_once("../libs/simple_html_dom.php");
        $this->content = file_get_html($this->url);
    }
    function createFolder() {
        require("../settings.php");
        mkdir($image_folder.$this->hash);
    }

    function addToDB() {
        require("../settings.php");
        $mysqli = new mysqli($DB_host, $DB_user, $DB_pass, $DB_name);
        if(mysqli_connect_errno()) {
            die(mysqli_connect_error());
        }
        $stmt = $mysqli->prepare("INSERT INTO pages(url, hash, created) VALUES (?,?,NOW())");
        $stmt->bind_param("ss",$this->url, $this->hash);
        $stmt->execute();
        $this->databaseID = $stmt->insert_id;
        $stmt->close();

    }

    function findImg() {
        $this->findTag("img");
    }

    function findTag($tagname) {
        require_once("../classes/Image.php");
        foreach($this->content->find($tagname) as $image) {
            $img = new Image($image->src, $image->title, $this->url,$this->hash,$this->databaseID);
            //$img->toString();
        }
    }

    public static function getPageHash($mysqli, $id) {
        $res = $mysqli->query("SELECT hash FROM pages WHERE id=".$id);
        return $res->fetch_object()->hash;
    }
    public static function getImageCount($mysqli, $id) {
        $res = $mysqli->query("SELECT * FROM images WHERE page_id=".$id);
        return $res->num_rows;
    }


}