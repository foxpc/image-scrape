<?php


class Image {
    var $url;
    var $name;
    var $title;
    var $dim_width;
    var $dim_height;
    var $type;
    var $size;
    var $hash;
    var $pageId;
    var $imageLocalUrl;

    function __construct($image_url, $image_title, $remote_url, $hash, $pageId)
    {
        //var_dump($image);
        $this->url = $image_url;
        if(!$this->isValidLink()) {
            $this->url = $remote_url ."/../". $this->url;
        }
        $this->title = $image_title;
        $this->hash = $hash;
        $this->pageId = $pageId;
        $this->downloadImage();
        $this->getMoreData();

        $this->addToDB();
    }

    function isValidLink() {
        return filter_var($this->url,FILTER_VALIDATE_URL);
    }

    function downloadImage() {
        $image_folder = 0; // IDE err fix
        require("../settings.php");
        $this->name = basename($this->url);
        $this->imageLocalUrl = $image_folder."/".$this->hash."/".$this->name;
        copy($this->url,$this->imageLocalUrl);
    }
    function getMoreData() {
        $image_folder = 0; // IDE err fix
        require("../settings.php");

        list($this->dim_width, $this->dim_height, $type) = getimagesize($this->imageLocalUrl);
        $this->type = $image_type[$type];

        $this->size = filesize($this->imageLocalUrl);
    }

    function addToDB() {
        require("../settings.php");
        $mysqli = new mysqli($DB_host, $DB_user, $DB_pass, $DB_name);
        if(mysqli_connect_errno()) {
            die(mysqli_connect_error());
        }
        $stmt = $mysqli->prepare("INSERT INTO images(name, page_id, type, size, width, height) VALUES (?,?,?,?,?,?)");
        $stmt->bind_param("sisiii",$this->name, $this->pageId, $this->type, $this->size, $this->dim_width, $this->dim_height);

        $stmt->execute();
        echo "err: " . $stmt->error;
        $stmt->close();

    }

    function toString() {
        echo "Image url: ".$this->url."; Title: ". $this->title . "; Width: ". $this->dim_width . "; Height: ". $this->dim_height. "; Type: " .$this->type . "; Size: " .$this->size . "\n";
    }


}