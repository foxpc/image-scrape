<?php
require_once("../classes/Url.php");

function getAllPages() {
    $mysqli = connectToDB();
    $res = $mysqli->query("SELECT * FROM pages ORDER BY id DESC");
    $output = "<table>";
    while($page = $res->fetch_object()) {
        //var_dump($page);
        $imageCount = URL::getImageCount($mysqli, $page->id);
        $output .= "<tr><td><a target='_blank' href='$page->url'>$page->url</a></td><td>$page->created</td><td><a href='#' class='show_images' id='$page->id'>Rodyti pav.</a></td><td>Kiekis: $imageCount</td></tr>";
    }
    echo $output;
}
function getSingleElement($id) {
    if(!is_numeric($id)) {
        echo "Nerastas skaičius. Negerai.";
        return;
    }
    $mysqli = connectToDB();

    $folderName = Url::getPageHash($mysqli, $id);


    $output = "<table border=1>";
    $res = $mysqli->query("SELECT * FROM images WHERE page_id=".$id);
    while($image = $res->fetch_object()) {
        //var_dump($image);
        $output .= "<tr><td><a target='_blank' href='../images/$folderName/$image->name'><img width=100 src='../images/$folderName/$image->name'></a></td>
                        <td>Dydis:$image->size baitų<br>Plotis:$image->width<br>Aukštis:$image->height<br>Tipas:$image->type</td>
                    </tr>";
    }
    echo $output;
}
function connectToDB() {
    require("../settings.php");
    $mysqli = new mysqli($DB_host, $DB_user, $DB_pass, $DB_name);
    if(mysqli_connect_errno()) {
        die(mysqli_connect_error());
    }
    return $mysqli;
}
if(isset($_GET['getAll'])) {
    getAllpages();
} else if(isset($_GET['getSingle'])){
    getSingleElement($_GET['getSingle']);
}


?>